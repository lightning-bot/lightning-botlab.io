# Bot Configuration

For additional help, join the support server [https://discord.gg/{{ l_docs.discord_invite_code }}](https://discord.gg/{{l_docs.discord_invite_code}})

## Customizing the bot's prefix

By default, the bot's prefix is it's mention (@Lightning) and cannot be removed.

| Command | Example | Usage |
| :--- | :--- | :--- |
| **config prefix** | -- | Sends the list of custom prefixes that can be used in the guild. |
| **config prefix add &lt;prefix&gt;** | .config prefix add "lightning " | Adds a prefix to be used by the bot \(limited to 10\) **NOTE:** If you want a two word prefix or a prefix with a space after it or an emoji you **must** use quotes, this is a discord limitation and can't be fixed. |
| **config prefix remove &lt;prefix&gt;** | .config prefix remove ! | Removes a prefix, same limits as .prefix add applies here, can't remove mentioning the bot or the default prefix (@Lightning). |


## Feature Flags

Certain features are disabled by default. You can toggle a feature on with `.config toggle <feature>`.

### InvokeDelete

Removes command invocation messages after successful execution.

You can toggle this feature on by passing "invoke delete" for the feature argument.

### RoleSaver

Saves a user's roles when they leave a server. When the user rejoins the server again, the bot will try to apply all the roles they had.

You can toggle this feature on or off by passing "role saver" for the feature argument.

## Logging

### Events

| Event | Description | Audit Log Integration |
| :----- | :---------- | :------------------- |
| Warn | Logs when the warn command is used. | Not Applicable |
| Kick | Logs when a member is kicked. | ✅ |
| Ban | Logs when a member is banned. | ✅ |
| Mute | Logs when the mute or timemute command is used. | Not Applicable |
| Unmute | Logs when the unmute command is used. | Not Applicable |
| Unban | Logs when a user is unbanned. | ✅ |
| Member Join | Logs when a member joins a server. | Not Applicable |
| Member Leave | Logs when a member leaves a server. | Not Applicable |
| Member Role Add | Logs when roles are added to a member | ✅ |
| Member Role Remove | Logs when roles are removed from a member | ✅ |
| Member Nick Change | Logs when a nickname is added/changed/removed from a member | ✅ |

### Logging Formats

The bot includes 4 formats in which you can receive logs.

=== "Minimalistic with Timestamp (default)"
    A simple, minimal format.

    ![Example Image](../assets/minimal.png)

=== "Minimalistic without Timestamp"
    Same as minimalistic except it doesn't include a timestamp.

    ![Example Image](../assets/minimal2.png)

=== "Embed"
    An embedded format aiming to be simple and without useless clutter.

    ![Example Image](../assets/embed.png)

=== "Emoji"
    A format based on Kurisu's style of logging.

    ![Example Image](../assets/emoji.png)

To configure the format, use `.config logging` and follow the prompts.


## Fine-Tuned Command Permissions

### Levels

Levels allow you set certain roles or members as a certain level in the bot.

To set up a level, use `.config permissions add <member/role>` to add and `.config permissions remove <member/role>` to remove.

#### Admin

Admins are the highest in power with Lightning. They can configure anything on the bot freely and use any command.

!!! danger
    You should only give people you trust the most the Admin level.

#### Mod

Moderators are the members of your server who police the server.
Mods cannot configure the bot, but they can use a range of commands that help moderate the server.

#### Trusted

Trusted are people in your server that you trust. They don't have access to moderation commands and cannot configure the bot. They are exempt from Auto-mod.


### Disabling commands

To disable a command from being run in your server, run `.config permissions blockcommand <command>`.

To undo this, run `.config permissions unblockcommand <command>`.

!!! warning
    No one, not even the server owner, can bypass this restriction unless they add a command override.

### Explicit Command Allows

Overrides the current permissions required for a command, so that the role or user can use the command.

To do this, run `.config permissions commandoverrides add <command> <user/role>`

If you want change the level for a command, run `.config permissions commandoverrides changelevel <command> <level>`

To undo all overrides for a command, run `.config permissions commandoverrides removeall <command>`

!!! warning
    The bot owner/devs are not responsible for any damage that may happen. Use it wisely.
