## pin <message_id> [channel=CurrentChannel]

Pins a message by ID.

#### Aliases

None


#### Arguments

• **message_id**: `int`

• **channel**: `TextChannel`




