## mute <target> [reason] [--duration] [--nodm]

Mutes a user

#### Aliases

None


#### Arguments

• **target**: `TargetMember`

• **flags**: `str`

#### Flags

`--duration, -D` (Time): Duration for the mute


`--nodm, --no-dm` (No argument): Bot does not DM the user the reason for the action.



