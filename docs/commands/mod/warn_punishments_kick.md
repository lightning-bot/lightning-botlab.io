## warn punishments kick <number>

Configures the warn kick punishment.<br><br>This kicks the member after acquiring a certain amount of warns.

#### Aliases

None


#### Arguments

• **number**: `InbetweenNumber`




