## warn <target> [reason] [--nodm]

Warns a user

#### Aliases

None


#### Arguments

• **target**: `TargetMember`

• **flags**: `str`

#### Flags

`--nodm, --no-dm` (No argument): Bot does not DM the user the reason for the action.


#### Subcommands

[`warn punishments`](https://lightning-bot.gitlab.io/commands/mod/warn_punishments)

