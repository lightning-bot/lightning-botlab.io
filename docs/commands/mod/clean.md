## clean [search=100] [channel=CurrentChannel]

Cleans the bot's messages from the channel specified.<br><br>If no channel is specified, the bot deletes its         messages from the channel the command was run in.<br><br>If a search number is specified, it will search         that many messages from the bot in the specified channel and clean them.

#### Aliases

None


#### Arguments

• **search**: `int`

• **channel**: `TextChannel`




