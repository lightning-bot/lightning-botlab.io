## purge attachments [search=100]

Removes messages that contains attachments in the message.

#### Aliases

`files`


#### Arguments

• **search**: `int`




