## warn punishments 

Configures warn punishments for the server.

#### Aliases

`punishment`


#### Arguments

None



#### Subcommands

[`warn punishments ban`](https://lightning-bot.gitlab.io/commands/mod/warn_punishments_ban), [`warn punishments kick`](https://lightning-bot.gitlab.io/commands/mod/warn_punishments_kick), [`warn punishments clear`](https://lightning-bot.gitlab.io/commands/mod/warn_punishments_clear)

