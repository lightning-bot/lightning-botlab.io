## unlock [channel=CurrentChannel]

Unlocks the channel mentioned.<br><br>If no channel was mentioned, it unlocks the channel the command was used in.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`



#### Subcommands

[`unlock hard`](https://lightning-bot.gitlab.io/commands/mod/unlock_hard)

