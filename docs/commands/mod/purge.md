## purge <search>

Purges messages that meet a certain criteria.<br><br>If called without a subcommand, the bot will remove all messages.

#### Aliases

`clear`


#### Arguments

• **search**: `int`



#### Subcommands

[`purge attachments`](https://lightning-bot.gitlab.io/commands/mod/purge_attachments), [`purge contains`](https://lightning-bot.gitlab.io/commands/mod/purge_contains), [`purge user`](https://lightning-bot.gitlab.io/commands/mod/purge_user)

