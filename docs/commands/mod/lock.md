## lock [channel=CurrentChannel]

Locks down the channel mentioned.<br><br>Sets the channel permissions as @everyone can't send messages.<br><br>If no channel was mentioned, it locks the channel the command was used in.

#### Aliases

`lockdown`


#### Arguments

• **channel**: `TextChannel`



#### Subcommands

[`lock hard`](https://lightning-bot.gitlab.io/commands/mod/lock_hard)

