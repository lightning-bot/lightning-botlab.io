## kick <target> [reason] [--nodm]

Kicks a user from the server

#### Aliases

None


#### Arguments

• **target**: `TargetMember`

• **flags**: `str`

#### Flags

`--nodm, --no-dm` (No argument): Bot does not DM the user the reason for the action.



