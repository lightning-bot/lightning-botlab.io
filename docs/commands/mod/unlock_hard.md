## unlock hard [channel=CurrentChannel]

Hard unlocks the channel mentioned.<br><br>If no channel was mentioned, it unlocks the channel the command was used in.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`




