## unban <member> [reason]

Unbans a user<br><br>You can pass either the ID of the banned member or the Name#Discrim         combination of the member. The member's ID is easier to use.

#### Aliases

None


#### Arguments

• **member**: `BannedMember`

• **reason**: `str`




