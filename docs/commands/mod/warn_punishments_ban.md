## warn punishments ban <number>

Configures the warn ban punishment.<br><br>This bans the member after acquiring a certain amount of warns or higher.

#### Aliases

None


#### Arguments

• **number**: `InbetweenNumber`




