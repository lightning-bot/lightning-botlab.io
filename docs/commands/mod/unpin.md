## unpin <message_id> [channel=CurrentChannel]

Unpins a message by ID.

#### Aliases

None


#### Arguments

• **message_id**: `int`

• **channel**: `TextChannel`




