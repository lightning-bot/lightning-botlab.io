## massban [members]... <reason>

Mass bans users from the server.<br><br>Note: Users will not be notified about being banned from the server.

#### Aliases

None


#### Arguments

• **members**: `_Greedy`

• **reason**: `str`




