## purge user <member> [search=100]

Removes messages from a member

#### Aliases

None


#### Arguments

• **member**: `Member`

• **search**: `int`




