## lock hard [channel=CurrentChannel]

Hard locks a channel.<br><br>Sets the channel permissions as @everyone can't         send messages or read messages in the channel.<br><br>If no channel was mentioned, it hard locks the channel the command was used in.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`




