## purge contains <string>

Removes messages containing a certain substring.

#### Aliases

None


#### Arguments

• **string**: `str`




