## timeban <target><duration> [reason] [--nodm]

Bans a user for a specified amount of time.<br><br>The duration can be a short time format such as "30d",         a more human duration format such as "until Monday at 7PM",         or a more concrete time format such as "2020-12-31".<br><br>Note that duration time is in UTC.

#### Aliases

`tempban`


#### Arguments

• **target**: `TargetMember`

• **duration**: `FutureTime`

• **flags**: `str`

#### Flags

`--nodm, --no-dm` (No argument): Bot does not DM the user the reason for the action.



