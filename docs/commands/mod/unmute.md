## unmute <target> [reason]

Unmutes a user

#### Aliases

None


#### Arguments

• **target**: `Member`

• **reason**: `str`




