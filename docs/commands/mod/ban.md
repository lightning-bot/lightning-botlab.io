## ban <target> [reason] [--delete-messages] [--nodm] [--duration]

Bans a user.

#### Aliases

None


#### Arguments

• **target**: `TargetMember`

• **flags**: `str`

#### Flags

`--delete-messages` (int): Delete message history from a specified amount of days (Max 7)


`--nodm, --no-dm` (No argument): Bot does not DM the user the reason for the action.


`--duration, --time, -t` (Time): Duration for the ban



