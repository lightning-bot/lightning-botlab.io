## remind hide <reminder_id>

Marks a reminder as "secret"<br><br>A secret reminder will not show the description if you list reminders in a server.<br>When it's time to remind you, the bot will DM you about your reminder.

#### Aliases

None


#### Arguments

• **reminder_id**: `int`




