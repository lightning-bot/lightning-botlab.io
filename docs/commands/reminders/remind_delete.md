## remind delete <reminder_id>

Deletes a reminder by ID.<br><br>You can get the ID of a reminder with {prefix}remind list<br><br>You must own the reminder to remove it

#### Aliases

`cancel`


#### Arguments

• **reminder_id**: `int`




