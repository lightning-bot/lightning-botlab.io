## remind <when>

Reminds you of something after a certain date.<br><br>The input can be any direct date (e.g. YYYY-MM-DD)<br>or a human readable offset.<br><br>Examples:<br>- "{prefix}remind in 2 days do essay" (2 days)<br>- "{prefix}remind 1 hour do dishes" (1 hour)<br>- "{prefix}remind 60s clean" (60 seconds)<br><br>Times are in UTC.

#### Aliases

`reminder`


#### Arguments

• **when**: `UserFriendlyTime`



#### Subcommands

[`remind clear`](https://lightning-bot.gitlab.io/commands/reminders/remind_clear), [`remind list`](https://lightning-bot.gitlab.io/commands/reminders/remind_list), [`remind delete`](https://lightning-bot.gitlab.io/commands/reminders/remind_delete), [`remind show`](https://lightning-bot.gitlab.io/commands/reminders/remind_show), [`remind hide`](https://lightning-bot.gitlab.io/commands/reminders/remind_hide)

