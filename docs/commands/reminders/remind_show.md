## remind show <reminder_id>

Unmarks a reminder from the "secret" status.

#### Aliases

None


#### Arguments

• **reminder_id**: `int`




