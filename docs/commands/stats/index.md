# Stats
Statistics related commands

| Name                                                                            | Aliases       | Description                                                  | Usage                        |
|---------------------------------------------------------------------------------|---------------|--------------------------------------------------------------|------------------------------|
| [socketstats](https://lightning-bot.gitlab.io/commands/stats/socketstats)       | None          | Shows a count of all tracked socket events                   | `.socketstats`               |
| [stats](https://lightning-bot.gitlab.io/commands/stats/stats)                   | None          | Sends stats about which commands are used often in the guild | `.stats [member]`            |
| [stats all](https://lightning-bot.gitlab.io/commands/stats/stats_all)           | None          | Sends stats on the most popular commands used in the bot     | `.stats all`                 |
| [stats auditlog](https://lightning-bot.gitlab.io/commands/stats/stats_auditlog) | `table` `log` | Shows command stats for the server through a table.          | `.stats auditlog [limit=50]` |

