## stats [member]

Sends stats about which commands are used often in the guild

#### Aliases

None


#### Arguments

• **member**: `Member`



#### Subcommands

[`stats all`](https://lightning-bot.gitlab.io/commands/stats/stats_all), [`stats auditlog`](https://lightning-bot.gitlab.io/commands/stats/stats_auditlog)

