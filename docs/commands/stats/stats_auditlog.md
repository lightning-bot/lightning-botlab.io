## stats auditlog [limit=50]

Shows command stats for the server through a table.

#### Aliases

`table, log`


#### Arguments

• **limit**: `InbetweenNumber`




