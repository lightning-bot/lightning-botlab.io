## togglerole <roles...>

Toggles a role that this server has setup.<br><br>To toggle multiple roles, you'll need to use a comma (",") as a separator.<br><br>Use "{prefix}togglerole list" for a list of roles that you can toggle.

#### Aliases

`selfrole`


#### Arguments

• **roles**: `str`



#### Subcommands

[`togglerole list`](https://lightning-bot.gitlab.io/commands/roles/togglerole_list), [`togglerole delete`](https://lightning-bot.gitlab.io/commands/roles/togglerole_delete), [`togglerole purge`](https://lightning-bot.gitlab.io/commands/roles/togglerole_purge), [`togglerole add`](https://lightning-bot.gitlab.io/commands/roles/togglerole_add)

