## togglerole add <role>

Adds a role to the list of toggleable roles for members

#### Aliases

`set`


#### Arguments

• **role**: `Role`




