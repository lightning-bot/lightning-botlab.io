## togglerole delete <role>

Removes a role from the toggleable role list

#### Aliases

`remove`


#### Arguments

• **role**: `_GenericAlias`




