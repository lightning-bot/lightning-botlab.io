## permissions [member=Author] [channel=CurrentChannel]

Shows channel permissions for a member

#### Aliases

None


#### Arguments

• **member**: `Member`

• **channel**: `TextChannel`




