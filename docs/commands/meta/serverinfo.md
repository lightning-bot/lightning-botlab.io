## serverinfo 

Shows information about the server

#### Aliases

`guildinfo`


#### Arguments

• **guild_id**: `GuildID`




