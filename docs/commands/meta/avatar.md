## avatar [member=Author]

Displays a user's avatar

#### Aliases

`avy`


#### Arguments

• **member**: `GuildorNonGuildUser`




