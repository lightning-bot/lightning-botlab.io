## join [ids...]

Gives you a link to add the bot to your server or generates an invite link for a client id.

#### Aliases

`invite`


#### Arguments

• **ids**: `ClientID`




