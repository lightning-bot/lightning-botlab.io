## roleinfo <role>

Gives information for a role

#### Aliases

None


#### Arguments

• **role**: `Role`




