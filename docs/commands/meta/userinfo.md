## userinfo [member=Author]

Displays information for a user

#### Aliases

`ui`


#### Arguments

• **member**: `GuildorNonGuildUser`




