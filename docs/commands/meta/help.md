## help [command]

Shows help about the bot, a command, or a category

#### Aliases

None


#### Arguments

• **command**: `str`




