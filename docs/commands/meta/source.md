## source [command]

Gives a link to the source code for a command.

#### Aliases

None


#### Arguments

• **command**: `str`




