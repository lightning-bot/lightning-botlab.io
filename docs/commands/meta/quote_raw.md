## quote raw [message...]

Shows raw JSON for a message.

#### Aliases

`json`


#### Arguments

• **message**: `str`




