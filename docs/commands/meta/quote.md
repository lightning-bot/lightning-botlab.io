## quote [message...]

Quotes a message

#### Aliases

`messageinfo, msgtext`


#### Arguments

• **message**: `str`



#### Subcommands

[`quote raw`](https://lightning-bot.gitlab.io/commands/meta/quote_raw)

