## config prefix remove <prefix>

Removes a custom prefix.<br><br>To remove word/multi-word prefixes, you need to quote it.<br><br>Example: `.prefix remove "lightning "` removes the "lightning " prefix.

#### Aliases

None


#### Arguments

• **prefix**: `Prefix`




