## config prefix 

Manages the server's custom prefixes.<br><br>If called without a subcommand, this will list the currently set prefixes for this server.

#### Aliases

`prefixes`


#### Arguments

None



#### Subcommands

[`config prefix remove`](https://lightning-bot.gitlab.io/commands/configuration/config_prefix_remove), [`config prefix add`](https://lightning-bot.gitlab.io/commands/configuration/config_prefix_add)

