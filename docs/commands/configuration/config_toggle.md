## config toggle <feature>

Toggles a feature flag

#### Aliases

None


#### Arguments

• **feature**: `convert_to_feature`




