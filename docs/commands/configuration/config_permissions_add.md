## config permissions add <level> <_id>

Adds a user to a level

#### Aliases

None


#### Arguments

• **level**: `convert_to_level`

• **_id**: `Member`




