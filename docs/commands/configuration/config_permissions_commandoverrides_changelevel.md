## config permissions commandoverrides changelevel <command> <level>

Overrides a command's level

#### Aliases

None


#### Arguments

• **command**: `ValidCommandName`

• **level**: `convert_to_level_value`




