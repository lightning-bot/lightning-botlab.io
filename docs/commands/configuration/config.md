## config 

Manages most of the configuration for the bot.<br><br>Manages:<br>  - Mute role<br>  - Logging<br>  - Prefixes<br>  - Command Overrides<br>  - Feature Flags<br>  - Levels

#### Aliases

None


#### Arguments

None



#### Subcommands

[`config logging`](https://lightning-bot.gitlab.io/commands/configuration/config_logging), [`config automod`](https://lightning-bot.gitlab.io/commands/configuration/config_automod), [`config prefix`](https://lightning-bot.gitlab.io/commands/configuration/config_prefix), [`config autorole`](https://lightning-bot.gitlab.io/commands/configuration/config_autorole), [`config permissions`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions), [`config toggle`](https://lightning-bot.gitlab.io/commands/configuration/config_toggle), [`config muterole`](https://lightning-bot.gitlab.io/commands/configuration/config_muterole)

