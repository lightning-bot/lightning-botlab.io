## config permissions commandoverrides add <command> <ids...>

Allows users/roles to run a command

#### Aliases

None


#### Arguments

• **command**: `ValidCommandName`

• **ids**: `_GenericAlias`




