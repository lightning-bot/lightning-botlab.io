## config permissions fallback <boolean>

Toggles the fallback permissions feature

#### Aliases

None


#### Arguments

• **boolean**: `bool`




