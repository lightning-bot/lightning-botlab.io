## config permissions remove <level> <_id>

Removes a user from a level

#### Aliases

None


#### Arguments

• **level**: `convert_to_level`

• **_id**: `_GenericAlias`




