## config logging [channel=CurrentChannel]

Sets up logging for the server.<br><br>This handles changing the log format for the server, removing logging from a channel, and setting up         logging for a channel.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`




