## config muterole reset  [rest] [--temp]

Deletes the configured mute role.

#### Aliases

`delete, remove`


#### Arguments

• **flags**: `str`

#### Flags

`--temp, -T` (No argument): Whether to remove the temp mute role



