## config permissions blockcommand <command>

Blocks a command to everyone.

#### Aliases

None


#### Arguments

• **command**: `ValidCommandName`




