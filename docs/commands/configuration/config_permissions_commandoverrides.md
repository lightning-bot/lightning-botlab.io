## config permissions commandoverrides 

Manages configuration for command overrides.<br><br>This allows you to allow certain people or roles to use a command without needing a role recognized as a level.

#### Aliases

None


#### Arguments

None



#### Subcommands

[`config permissions commandoverrides removeall`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_commandoverrides_removeall), [`config permissions commandoverrides reset`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_commandoverrides_reset), [`config permissions commandoverrides changelevel`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_commandoverrides_changelevel), [`config permissions commandoverrides add`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_commandoverrides_add)

