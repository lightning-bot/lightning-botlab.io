## config permissions commandoverrides removeall <command>

Removes all overrides from a command

#### Aliases

None


#### Arguments

• **command**: `ValidCommandName`




