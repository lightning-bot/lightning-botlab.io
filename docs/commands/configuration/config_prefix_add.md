## config prefix add <prefix>

Adds a custom prefix.<br><br>To have a prefix with a word (or words), you should quote it and         end it with a space, e.g. "lightning " to set the prefix         to "lightning ". This is because Discord removes spaces when sending         messages so the spaces are not preserved.

#### Aliases

None


#### Arguments

• **prefix**: `Prefix`




