## config autorole 

Manages the server's autorole

#### Aliases

None


#### Arguments

None



#### Subcommands

[`config autorole remove`](https://lightning-bot.gitlab.io/commands/configuration/config_autorole_remove), [`config autorole set`](https://lightning-bot.gitlab.io/commands/configuration/config_autorole_set)

