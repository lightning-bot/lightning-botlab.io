## config autorole set <role>

Sets an auto role for the server

#### Aliases

`add`


#### Arguments

• **role**: `Role`




