## config permissions 

Manages user permissions for the bot

#### Aliases

None


#### Arguments

None



#### Subcommands

[`config permissions commandoverrides`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_commandoverrides), [`config permissions fallback`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_fallback), [`config permissions debug`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_debug), [`config permissions add`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_add), [`config permissions unblockcommand`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_unblockcommand), [`config permissions reset`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_reset), [`config permissions blockcommand`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_blockcommand), [`config permissions remove`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_remove), [`config permissions show`](https://lightning-bot.gitlab.io/commands/configuration/config_permissions_show)

