## config muterole update  [rest] [--temp]

Updates the permission overwrites of the mute role.<br><br>This sets the permissions to Send Messages and Add Reactions as False<br>on every text channel that the bot can set permissions for.

#### Aliases

None


#### Arguments

• **flags**: `str`

#### Flags

`--temp, -T` (No argument): Whether to use the temp mute role



