## config permissions debug <command> [member=Author]

Debugs a member's permissions to use a command.

#### Aliases

None


#### Arguments

• **command**: `ValidCommandName`

• **member**: `Member`




