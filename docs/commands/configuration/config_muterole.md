## config muterole [role]

Handles mute role configuration.<br><br>This command allows you to set the mute role for the server or view the configured mute role.

#### Aliases

None


#### Arguments

• **role**: `Role`



#### Subcommands

[`config muterole reset`](https://lightning-bot.gitlab.io/commands/configuration/config_muterole_reset), [`config muterole temp`](https://lightning-bot.gitlab.io/commands/configuration/config_muterole_temp), [`config muterole update`](https://lightning-bot.gitlab.io/commands/configuration/config_muterole_update), [`config muterole unbind`](https://lightning-bot.gitlab.io/commands/configuration/config_muterole_unbind)

