## nitroemoji <emoji>

Posts either an animated emoji or non-animated emoji if found<br><br>Note: The server which the emoji is from must be approved by the bot owner.

#### Aliases

`nemoji, nitro`


#### Arguments

• **emoji**: `str`




