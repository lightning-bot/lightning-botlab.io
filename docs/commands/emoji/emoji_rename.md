## emoji rename <old_name> <new_name>

Renames an emoji from the server

#### Aliases

None


#### Arguments

• **old_name**: `Emoji`

• **new_name**: `str`




