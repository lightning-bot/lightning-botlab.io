## emoji 

Emoji management commands

#### Aliases

`emote`


#### Arguments

None



#### Subcommands

[`emoji delete`](https://lightning-bot.gitlab.io/commands/emoji/emoji_delete), [`emoji add`](https://lightning-bot.gitlab.io/commands/emoji/emoji_add), [`emoji info`](https://lightning-bot.gitlab.io/commands/emoji/emoji_info), [`emoji list`](https://lightning-bot.gitlab.io/commands/emoji/emoji_list), [`emoji approvenitro`](https://lightning-bot.gitlab.io/commands/emoji/emoji_approvenitro), [`emoji rename`](https://lightning-bot.gitlab.io/commands/emoji/emoji_rename)

