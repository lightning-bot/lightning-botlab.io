## emoji delete <emote>

Deletes an emoji from the server

#### Aliases

None


#### Arguments

• **emote**: `Emoji`




