## emoji info <emote>

Gives some info on an emote.<br><br>Unicode emoji are not supported.

#### Aliases

None


#### Arguments

• **emote**: `EmojiRE`




