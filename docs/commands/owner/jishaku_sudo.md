## jishaku sudo <command_string>

Run a command bypassing all checks and cooldowns.<br><br>This also bypasses permission checks so this has a high possibility of making commands raise exceptions.

#### Aliases

None


#### Arguments

• **command_string**: `str`




