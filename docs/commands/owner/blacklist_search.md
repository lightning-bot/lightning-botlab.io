## blacklist search <user_id>

Search the blacklist to see if a user is blacklisted

#### Aliases

None


#### Arguments

• **user_id**: `int`




