## jishaku in <channel> <command_string>

Run a command as if it were run in a different channel.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`

• **command_string**: `str`




