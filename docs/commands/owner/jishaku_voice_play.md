## jishaku voice play <uri>

Plays audio direct from a URI.<br><br>Can be either a local file or an audio resource on the internet.

#### Aliases

`play_local`


#### Arguments

• **uri**: `str`




