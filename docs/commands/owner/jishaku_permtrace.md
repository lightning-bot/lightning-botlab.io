## jishaku permtrace <channel> [targets...]

Calculates the source of granted or rejected permissions.<br><br>This accepts a channel, and either a member or a list of roles.<br>It calculates permissions the same way Discord does, while keeping track of the source.

#### Aliases

None


#### Arguments

• **channel**: `_GenericAlias`

• **targets**: `_GenericAlias`




