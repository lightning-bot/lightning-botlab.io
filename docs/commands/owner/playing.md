## playing [gamename]

Sets the bot's playing message.

#### Aliases

`status`


#### Arguments

• **gamename**: `str`




