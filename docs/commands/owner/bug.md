## bug 

Commands to manage the bug system

#### Aliases

None


#### Arguments

None



#### Subcommands

[`bug view`](https://lightning-bot.gitlab.io/commands/owner/bug_view), [`bug delete`](https://lightning-bot.gitlab.io/commands/owner/bug_delete), [`bug list`](https://lightning-bot.gitlab.io/commands/owner/bug_list)

