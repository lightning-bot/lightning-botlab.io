## jishaku voice disconnect 

Disconnects from the voice channel in this guild, if there is one.

#### Aliases

`dc`


#### Arguments

None




