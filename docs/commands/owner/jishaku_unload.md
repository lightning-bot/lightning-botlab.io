## jishaku unload [extensions...]

Unloads the given extension names.<br><br>Reports any extensions that failed to unload.

#### Aliases

None


#### Arguments

• **extensions**: `ExtensionConverter`




