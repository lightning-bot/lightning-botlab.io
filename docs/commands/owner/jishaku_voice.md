## jishaku voice 

Voice-related commands.<br><br>If invoked without subcommand, relays current voice state.

#### Aliases

`vc`


#### Arguments

None



#### Subcommands

[`jishaku voice disconnect`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_disconnect), [`jishaku voice volume`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_volume), [`jishaku voice play`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_play), [`jishaku voice pause`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_pause), [`jishaku voice join`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_join), [`jishaku voice resume`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_resume), [`jishaku voice stop`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice_stop)

