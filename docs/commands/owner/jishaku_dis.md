## jishaku dis <argument>

Disassemble Python code into bytecode.

#### Aliases

`disassemble`


#### Arguments

• **argument**: `codeblock_converter`




