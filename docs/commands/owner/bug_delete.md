## bug delete <token>

Deletes a bug if it exists

#### Aliases

`remove, rm`


#### Arguments

• **token**: `str`




