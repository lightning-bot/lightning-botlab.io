## jishaku retain [toggle]

Turn variable retention for REPL on or off.<br><br>Provide no argument for current status.

#### Aliases

None


#### Arguments

• **toggle**: `bool`




