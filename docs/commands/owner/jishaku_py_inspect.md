## jishaku py_inspect <argument>

Evaluation of Python code with inspect information.

#### Aliases

`pyi, python_inspect, pythoninspect`


#### Arguments

• **argument**: `codeblock_converter`




