## bug list [limit=10]

Lists the most recent bugs

#### Aliases

`recent`


#### Arguments

• **limit**: `int`




