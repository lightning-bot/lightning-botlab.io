## jishaku leaveguild <guild_id>

Leaves a guild that the bot is in via ID

#### Aliases

None


#### Arguments

• **guild_id**: `int`




