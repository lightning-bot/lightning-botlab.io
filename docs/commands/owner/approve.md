## approve <guild_id>

Approves a server.<br><br>Server must have already existed in the database before.

#### Aliases

None


#### Arguments

• **guild_id**: `int`




