## jishaku source <command_name>

Displays the source code for a command.

#### Aliases

`src`


#### Arguments

• **command_name**: `str`




