## jishaku debug <command_string>

Run a command timing execution and catching exceptions.

#### Aliases

`dbg`


#### Arguments

• **command_string**: `str`




