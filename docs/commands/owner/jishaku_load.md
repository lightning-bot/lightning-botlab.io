## jishaku load [extensions...]

Loads or reloads the given extension names.<br><br>Reports any extensions that failed to load.

#### Aliases

`reload`


#### Arguments

• **extensions**: `ExtensionConverter`




