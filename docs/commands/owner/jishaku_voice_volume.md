## jishaku voice volume <percentage>

Adjusts the volume of an audio source if it is supported.

#### Aliases

None


#### Arguments

• **percentage**: `float`




