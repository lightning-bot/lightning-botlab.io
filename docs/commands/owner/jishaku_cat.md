## jishaku cat <argument>

Read out a file, using syntax highlighting if detected.<br><br>Lines and linespans are supported by adding '#L12' or '#L12-14' etc to the end of the filename.

#### Aliases

None


#### Arguments

• **argument**: `str`




