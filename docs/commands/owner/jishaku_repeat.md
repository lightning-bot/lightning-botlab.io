## jishaku repeat <times> <command_string>

Runs a command multiple times in a row.<br><br>This acts like the command was invoked several times manually, so it obeys cooldowns.<br>You can use this in conjunction with `jsk sudo` to bypass this.

#### Aliases

None


#### Arguments

• **times**: `int`

• **command_string**: `str`




