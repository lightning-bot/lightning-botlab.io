## jishaku su <target> <command_string>

Run a command as someone else.<br><br>This will try to resolve to a Member, but will use a User if it can't find one.

#### Aliases

None


#### Arguments

• **target**: `User`

• **command_string**: `str`




