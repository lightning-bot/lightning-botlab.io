## blacklist adduser <user_id> [reason=No Reason Provided]

Blacklist an user from using the bot

#### Aliases

None


#### Arguments

• **user_id**: `int`

• **reason**: `str`




