## jishaku curl <url>

Download and display a text file from the internet.<br><br>This command is similar to jsk cat, but accepts a URL.

#### Aliases

None


#### Arguments

• **url**: `str`




