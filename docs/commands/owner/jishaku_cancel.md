## jishaku cancel <index>

Cancels a task with the given index.<br><br>If the index passed is -1, will cancel the last task instead.

#### Aliases

None


#### Arguments

• **index**: `_GenericAlias`




