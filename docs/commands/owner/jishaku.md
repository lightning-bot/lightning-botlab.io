## jishaku 

The Jishaku debug and diagnostic commands.<br><br>This command on its own gives a status brief.<br>All other functionality is within its subcommands.

#### Aliases

`jsk`


#### Arguments

None



#### Subcommands

[`jishaku voice`](https://lightning-bot.gitlab.io/commands/owner/jishaku_voice), [`jishaku leaveguild`](https://lightning-bot.gitlab.io/commands/owner/jishaku_leaveguild), [`jishaku pip`](https://lightning-bot.gitlab.io/commands/owner/jishaku_pip), [`jishaku hide`](https://lightning-bot.gitlab.io/commands/owner/jishaku_hide), [`jishaku su`](https://lightning-bot.gitlab.io/commands/owner/jishaku_su), [`jishaku show`](https://lightning-bot.gitlab.io/commands/owner/jishaku_show), [`jishaku in`](https://lightning-bot.gitlab.io/commands/owner/jishaku_in), [`jishaku tasks`](https://lightning-bot.gitlab.io/commands/owner/jishaku_tasks), [`jishaku sudo`](https://lightning-bot.gitlab.io/commands/owner/jishaku_sudo), [`jishaku cancel`](https://lightning-bot.gitlab.io/commands/owner/jishaku_cancel), [`jishaku repeat`](https://lightning-bot.gitlab.io/commands/owner/jishaku_repeat), [`jishaku unload`](https://lightning-bot.gitlab.io/commands/owner/jishaku_unload), [`jishaku load`](https://lightning-bot.gitlab.io/commands/owner/jishaku_load), [`jishaku cat`](https://lightning-bot.gitlab.io/commands/owner/jishaku_cat), [`jishaku debug`](https://lightning-bot.gitlab.io/commands/owner/jishaku_debug), [`jishaku retain`](https://lightning-bot.gitlab.io/commands/owner/jishaku_retain), [`jishaku source`](https://lightning-bot.gitlab.io/commands/owner/jishaku_source), [`jishaku shutdown`](https://lightning-bot.gitlab.io/commands/owner/jishaku_shutdown), [`jishaku rtt`](https://lightning-bot.gitlab.io/commands/owner/jishaku_rtt), [`jishaku curl`](https://lightning-bot.gitlab.io/commands/owner/jishaku_curl), [`jishaku permtrace`](https://lightning-bot.gitlab.io/commands/owner/jishaku_permtrace), [`jishaku git`](https://lightning-bot.gitlab.io/commands/owner/jishaku_git), [`jishaku py`](https://lightning-bot.gitlab.io/commands/owner/jishaku_py), [`jishaku py_inspect`](https://lightning-bot.gitlab.io/commands/owner/jishaku_py_inspect), [`jishaku dis`](https://lightning-bot.gitlab.io/commands/owner/jishaku_dis), [`jishaku shell`](https://lightning-bot.gitlab.io/commands/owner/jishaku_shell)

