## jishaku shell <argument>

Executes statements in the system shell.<br><br>This uses the system shell as defined in $SHELL, or `/bin/bash` otherwise.<br>Execution can be cancelled by closing the paginator.

#### Aliases

`bash, sh, powershell, ps1, ps, cmd`


#### Arguments

• **argument**: `codeblock_converter`




