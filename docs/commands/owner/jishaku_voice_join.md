## jishaku voice join [destination]

Joins a voice channel, or moves to it if already connected.<br><br>Passing a voice channel uses that voice channel.<br>Passing a member will use that member's current voice channel.<br>Passing nothing will use the author's voice channel.

#### Aliases

`connect`


#### Arguments

• **destination**: `_GenericAlias`




