## xkcd [value]

Shows an xkcd comic.<br><br>If no value is supplied or the value isn't found, it gives the latest xkcd instead.

#### Aliases

None


#### Arguments

• **value**: `int`




