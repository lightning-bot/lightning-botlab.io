## owoify  [rest] [--lastmessage] [--random]

An owo-ifier

#### Aliases

`owo, uwuify`


#### Arguments

• **args**: `str`

#### Flags

`--lastmessage, --lm` (No argument): Owoifies the last message sent in the channel


`--random, -R` (No argument): Owoifies random text from the last 20 messages in this channel



