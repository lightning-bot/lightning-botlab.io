## blacklightning episode <season> <episode>

Gives info on a certain episode of Black Lightning

#### Aliases

None


#### Arguments

• **season**: `int`

• **episode**: `int`




