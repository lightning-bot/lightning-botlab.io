## crate browse <crate>

Searches for a crate

#### Aliases

`search`


#### Arguments

• **crate**: `str`




