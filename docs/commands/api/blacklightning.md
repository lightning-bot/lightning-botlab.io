## blacklightning <season>

Gives summaries for episodes of Black Lightning.

#### Aliases

None


#### Arguments

• **season**: `int`



#### Subcommands

[`blacklightning episode`](https://lightning-bot.gitlab.io/commands/api/blacklightning_episode)

