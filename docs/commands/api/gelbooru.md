## gelbooru  [rest] [--limit]

Searches images on gelbooru

#### Aliases

None


#### Arguments

• **flags**: `str`

#### Flags

`--limit, -l` (int): How many results to search for



