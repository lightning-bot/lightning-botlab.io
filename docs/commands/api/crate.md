## crate 

Crate related commands.<br><br>Rewrite it in rust

#### Aliases

None


#### Arguments

None



#### Subcommands

[`crate browse`](https://lightning-bot.gitlab.io/commands/api/crate_browse), [`crate get`](https://lightning-bot.gitlab.io/commands/api/crate_get)

