# API
Commands that interact with different APIs

| Name                                                                                          | Aliases   | Description                                                                  | Usage                                        |
|-----------------------------------------------------------------------------------------------|-----------|------------------------------------------------------------------------------|----------------------------------------------|
| [blacklightning](https://lightning-bot.gitlab.io/commands/api/blacklightning)                 | None      | Gives summaries for episodes of Black Lightning.                             | `.blacklightning <season>`                   |
| [blacklightning episode](https://lightning-bot.gitlab.io/commands/api/blacklightning_episode) | None      | Gives info on a certain episode of Black Lightning                           | `.blacklightning episode <season> <episode>` |
| [crate](https://lightning-bot.gitlab.io/commands/api/crate)                                   | None      | Crate related commands.<br><br>Rewrite it in rust                            | `.crate`                                     |
| [crate browse](https://lightning-bot.gitlab.io/commands/api/crate_browse)                     | `search`  | Searches for a crate                                                         | `.crate browse <crate>`                      |
| [crate get](https://lightning-bot.gitlab.io/commands/api/crate_get)                           | None      |                                                                              | `.crate get <crate>`                         |
| [gelbooru](https://lightning-bot.gitlab.io/commands/api/gelbooru)                             | None      | Searches images on gelbooru                                                  | `.gelbooru  [rest] [--limit]`                |
| [neko](https://lightning-bot.gitlab.io/commands/api/neko)                                     | None      |                                                                              | `.neko`                                      |
| [qr](https://lightning-bot.gitlab.io/commands/api/qr)                                         | None      | Generates a QR code                                                          | `.qr <text>`                                 |
| [rtfs](https://lightning-bot.gitlab.io/commands/api/rtfs)                                     | None      | Shows source for an entity in discord.py<br><br>Read the fucking source nerd | `.rtfs <entity>`                             |

