## memes 

Runs a meme command.<br><br>If no meme is given, it sends a list of memes.

#### Aliases

`meme`


#### Arguments

None



#### Subcommands

[`memes catto`](https://lightning-bot.gitlab.io/commands/memes/memes_catto), [`memes cat`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_cat), [`memes git`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_git), [`memes jerry`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_jerry), [`memes givedsicoin`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_givedsicoin), [`memes bait`](https://lightning-bot.gitlab.io/commands/memes/memes_bait), [`memes lmao`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_lmao), [`memes star`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_star), [`memes discordcopypaste`](https://lightning-bot.gitlab.io/commands/memes/memes_discordcopypaste), [`memes police`](https://lightning-bot.gitlab.io/commands/not_categorized/memes_police)

