## memes discordcopypaste [member=Author]

Generates a discord copypaste<br><br>If no arguments are passed, it uses the author of the command.<br><br>If you fall for this, you should give yourself a solid facepalm.

#### Aliases

`discordcopypasta`


#### Arguments

• **member**: `Member`




