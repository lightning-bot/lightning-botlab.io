## mod 

Gets console modding information<br><br><br>If any information provided in the commands is incorrect,<br>please make an issue on the GitLab repository.<br>(https://gitlab.com/lightning-bot/Lightning)

#### Aliases

None


#### Arguments

None



#### Subcommands

[`mod 3ds`](https://lightning-bot.gitlab.io/commands/homebrew/mod_3ds), [`mod ds`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds), [`mod switch`](https://lightning-bot.gitlab.io/commands/homebrew/mod_switch), [`mod wii`](https://lightning-bot.gitlab.io/commands/homebrew/mod_wii)

