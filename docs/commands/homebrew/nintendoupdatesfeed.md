## nintendoupdatesfeed 

Manages the guild's configuration for Nintendo console update alerts.<br><br>If invoked with no subcommands, this will show the current configuration.

#### Aliases

`nuf, stability`


#### Arguments

None



#### Subcommands

[`nintendoupdatesfeed delete`](https://lightning-bot.gitlab.io/commands/homebrew/nintendoupdatesfeed_delete), [`nintendoupdatesfeed setup`](https://lightning-bot.gitlab.io/commands/homebrew/nintendoupdatesfeed_setup)

