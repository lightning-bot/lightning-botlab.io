## mod ds [homebrew]

Gives information on DS modding

#### Aliases

`DS, dsi, DSi`


#### Arguments

• **homebrew**: `str`



#### Subcommands

[`mod ds gba`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_gba), [`mod ds nesDS`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_nesDS), [`mod ds twilightmenu++`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_twilightmenu++), [`mod ds lolsnes`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_lolsnes), [`mod ds nds-bootstrap`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_nds-bootstrap), [`mod ds relaunch`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_relaunch), [`mod ds flashcard`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_flashcard), [`mod ds pkmn-chest`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_pkmn-chest), [`mod ds rocketvideoplayer`](https://lightning-bot.gitlab.io/commands/homebrew/mod_ds_rocketvideoplayer)

