## mod 3ds [homebrew]

Gives information on 3DS modding.

#### Aliases

`3d, 3DS, 2DS, 2ds`


#### Arguments

• **homebrew**: `str`



#### Subcommands

[`mod 3ds universal-updater`](https://lightning-bot.gitlab.io/commands/homebrew/mod_3ds_universal-updater)

