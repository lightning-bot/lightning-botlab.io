## nintendoupdatesfeed setup [channel=CurrentChannel]

Sets up a webhook in the specified channel that will send Nintendo console updates.

#### Aliases

None


#### Arguments

• **channel**: `TextChannel`




