## infraction list 

Lists infractions for the server

#### Aliases

None


#### Arguments

None



#### Subcommands

[`infraction list member`](https://lightning-bot.gitlab.io/commands/infractions/infraction_list_member), [`infraction list takenby`](https://lightning-bot.gitlab.io/commands/infractions/infraction_list_takenby)

