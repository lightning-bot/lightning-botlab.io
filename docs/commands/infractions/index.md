# Infractions
Infraction related commands

| Name                                                                                                    | Aliases   | Description                                    | Usage                                        |
|---------------------------------------------------------------------------------------------------------|-----------|------------------------------------------------|----------------------------------------------|
| [infraction](https://lightning-bot.gitlab.io/commands/infractions/infraction)                           | `inf`     |                                                | `.infraction`                                |
| [infraction claim](https://lightning-bot.gitlab.io/commands/infractions/infraction_claim)               | None      | Claims responsibility for an infraction        | `.infraction claim <infraction_id>`          |
| [infraction delete](https://lightning-bot.gitlab.io/commands/infractions/infraction_delete)             | `remove`  | Deletes an infraction                          | `.infraction delete <infraction_id>`         |
| [infraction edit](https://lightning-bot.gitlab.io/commands/infractions/infraction_edit)                 | None      | Edits the reason for an infraction             | `.infraction edit <infraction_id> <reason>`  |
| [infraction export](https://lightning-bot.gitlab.io/commands/infractions/infraction_export)             | None      | Exports the server's infractions to a JSON     | `.infraction export`                         |
| [infraction list](https://lightning-bot.gitlab.io/commands/infractions/infraction_list)                 | None      | Lists infractions for the server               | `.infraction list`                           |
| [infraction list member](https://lightning-bot.gitlab.io/commands/infractions/infraction_list_member)   | None      | Lists infractions done to a user               | `.infraction list member <member>`           |
| [infraction list takenby](https://lightning-bot.gitlab.io/commands/infractions/infraction_list_takenby) | None      | Lists infractions taken by a moderator         | `.infraction list takenby [member=Author]`   |
| [infraction transfer](https://lightning-bot.gitlab.io/commands/infractions/infraction_transfer)         | None      | Transfers a user's infractions to another user | `.infraction transfer <old_user> <new_user>` |
| [infraction view](https://lightning-bot.gitlab.io/commands/infractions/infraction_view)                 | None      | Views an infraction                            | `.infraction view <infraction_id>`           |

