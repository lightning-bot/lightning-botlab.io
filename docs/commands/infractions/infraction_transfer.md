## infraction transfer <old_user> <new_user>

Transfers a user's infractions to another user

#### Aliases

None


#### Arguments

• **old_user**: `TargetMember`

• **new_user**: `TargetMember`




