## infraction edit <infraction_id> <reason>

Edits the reason for an infraction

#### Aliases

None


#### Arguments

• **infraction_id**: `int`

• **reason**: `str`




