## infraction delete <infraction_id>

Deletes an infraction

#### Aliases

`remove`


#### Arguments

• **infraction_id**: `int`




