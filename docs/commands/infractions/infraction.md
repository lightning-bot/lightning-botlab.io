## infraction 



#### Aliases

`inf`


#### Arguments

None



#### Subcommands

[`infraction edit`](https://lightning-bot.gitlab.io/commands/infractions/infraction_edit), [`infraction delete`](https://lightning-bot.gitlab.io/commands/infractions/infraction_delete), [`infraction transfer`](https://lightning-bot.gitlab.io/commands/infractions/infraction_transfer), [`infraction claim`](https://lightning-bot.gitlab.io/commands/infractions/infraction_claim), [`infraction view`](https://lightning-bot.gitlab.io/commands/infractions/infraction_view), [`infraction export`](https://lightning-bot.gitlab.io/commands/infractions/infraction_export), [`infraction list`](https://lightning-bot.gitlab.io/commands/infractions/infraction_list)

