## infraction claim <infraction_id>

Claims responsibility for an infraction

#### Aliases

None


#### Arguments

• **infraction_id**: `int`




