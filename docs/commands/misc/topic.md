## topic [channel=CurrentChannel]

Quotes a channel's topic

#### Aliases

None


#### Arguments

• **channel**: `ReadableChannel`




