## archive <limit> [channel=CurrentChannel]

Archives the current channel's contents to a txt file.

#### Aliases

None


#### Arguments

• **limit**: `int`

• **channel**: `ReadableChannel`




