## poll <question>

Creates a simple poll with thumbs up, thumbs down, and shrug as reactions

#### Aliases

None


#### Arguments

• **question**: `str`




