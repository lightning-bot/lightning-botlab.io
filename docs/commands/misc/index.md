# Misc
Commands that might be helpful...

| Name                                                                       | Aliases   | Description                                                               | Usage                                       |
|----------------------------------------------------------------------------|-----------|---------------------------------------------------------------------------|---------------------------------------------|
| [archive](https://lightning-bot.gitlab.io/commands/misc/archive)           | None      | Archives the current channel's contents to a txt file.                    | `.archive <limit> [channel=CurrentChannel]` |
| [embedbuilder](https://lightning-bot.gitlab.io/commands/misc/embedbuilder) | None      | WIP embed builder command                                                 | `.embedbuilder`                             |
| [poll](https://lightning-bot.gitlab.io/commands/misc/poll)                 | None      | Creates a simple poll with thumbs up, thumbs down, and shrug as reactions | `.poll <question>`                          |
| [snowflake](https://lightning-bot.gitlab.io/commands/misc/snowflake)       | None      | Tells you when a snowflake was created                                    | `.snowflake <snowflake>`                    |
| [topic](https://lightning-bot.gitlab.io/commands/misc/topic)               | None      | Quotes a channel's topic                                                  | `.topic [channel=CurrentChannel]`           |

