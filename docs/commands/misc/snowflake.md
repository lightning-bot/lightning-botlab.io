## snowflake <snowflake>

Tells you when a snowflake was created

#### Aliases

None


#### Arguments

• **snowflake**: `str`




