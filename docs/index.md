# Lightning Bot

Source code: <https://gitlab.com/lightning-bot/Lightning>

Invite me to your guild: <https://discordapp.com/oauth2/authorize?client_id=532220480577470464&scope=bot&permissions=2013654230>

Support Server: <https://discord.gg/{{l_docs.discord_invite_code}}>


### :octicons-terminal-16: Commands
- [:globe_with_meridians: API](commands/api)
- [:pencil2: Config](commands/config)
- [:smiley: Emoji](commands/emoji)
- [:partying_face: Fun](commands/fun)
- [:video_game: Homebrew](commands/homebrew)
- [:tools: Infractions](commands/infractions)
- [:information_source: Meta](commands/meta)
- [:toolbox: Moderation](commands/moderation)
- [:alarm_clock: Reminders](commands/reminders)
- [:books: Roles](commands/roles)
- [:chart_with_upwards_trend: Stats](commands/stats)

### [:fontawesome-regular-edit: Configuration](bot-configuration)


!!! tip
    This documentation uses `.` as a prefix. Replace this with whatever prefix you have set up Lightning with.

