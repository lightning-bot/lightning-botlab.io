# Automod Configuration

Lightning's automod can be setup via a .toml file.

For more information about TOML, see https://toml.io/en/.

---

A base example of setting up message spam

```toml
[automod.message-spam]
count = 4
seconds = 5.0

[automod.message-spam.punishment]
type = 4
```

## Automod Types

| Name | Description |
| ---- | ----------- |
| message-spam | Automod type for controlling how many messages can a user send in the server during x seconds |
| mass-mentions | Automod type for controlling how many mentions can be sent in a single message |
| url-spam | Automod type for controlling how many links can be sent during x seconds |
| invite-spam | Automod type for controlling how many invites can be sent during x seconds |
| message-content-spam | Automod type for controlling how many messages containing the same content can be sent during x seconds |

## Punishment Configuration

You can also set up punishments that'll be handed out when a threshold is met for a certain type.

### Punishment Types

| Type | Number |
| ---- | ------ |
| DELETE | 1    |
| WARN | 2      |
| MUTE | 3      |
| KICK | 4      |
| BAN  | 5      |

!!! note
    DELETE is not supported as a message-spam punishment.

## Configuration Reference

| Key | Description | Type | Required? |
| --- | ----------- | ---- | --------- |
| type | The type of automod being configured, i.e. "message-spam" | str | Yes |
| count | How many times should an action be performed before the bot takes action | int | Yes |
| seconds |  How many seconds within which the "count" of actions should be performed to cause a punishment to be done | float | Yes |
| punishment | The punishment configuration options | [table](#punishment-configuration-reference) | Yes |

!!! note
    The mass-mentions type does not require the seconds field.

### Punishment Configuration Reference

| Key | Description | Type | Required? |
| --- | ----------- | ---- | --------- |
| type | The type of punishment to use | int | Yes |
| duration | The duration of the punishment type. **Only supported for BAN & MUTE** | str | No |

!!! note
    If you specify BAN or MUTE as the punishment type and don't specify a duration, the action will be permanent.
