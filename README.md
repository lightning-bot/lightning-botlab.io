# Docs

You can view the documentation online at [https://lightning-bot.gitlab.io](https://lightning-bot.gitlab.io).

## Building the Docs

You'll need python3.7 or above

Install the requirements. `pip install -U -r requirements.txt`

Run `mkdocs serve` to test changes if needed.

Run `mkdocs build` to build the docs.

## Credits

`Offline_bolt-24px.svg` is from Google's Material Design Icons [https://github.com/google/material-design-icons](https://github.com/google/material-design-icons).